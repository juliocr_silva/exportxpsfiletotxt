﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Xps.Packaging;

namespace Export
{
    public partial class Exportar : Form
    {
        public Exportar()
        {
            InitializeComponent();
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                Export();
            }
            catch (Exception ex)
            {
                lblStatus.Text = @"Falha.";
                MessageBox.Show($"Erro no processamento: {ex.Message} ({ex?.InnerException})");
            }
        }

        private void Export()
        {
            if (!IsValid()) return;

            var lData = ReadXpsFile();
            int columns;
            int.TryParse(editTotalColuna.Text, out columns);
            var sb = new StringBuilder();

            var count = 1;

            progressBar.Maximum = lData.Count;
            progressBar.Value = 0;

            foreach (var strItem in lData)
            {
                if (string.IsNullOrEmpty(strItem.Trim()))
                    continue;

                // ReSharper disable once PossiblyMistakenUseOfParamsMethod
                var str = string.Concat($"{strItem.Trim()}{"; "}");

                if (count % (columns) == 0)
                {
                    sb.AppendLine(str);
                }
                else
                {
                    sb.Append(str);
                }

                count++;
                progressBar.Value = count - 1;
            }

            File.WriteAllText(txtSaveAs.Text, sb.ToString(), Encoding.Default);
            lblStatus.Text = @"Exportação finalizada.";
        }

        private bool IsValid()
        {
            if (string.IsNullOrEmpty(txtFileDir.Text.Trim()))
            {
                MessageBox.Show(@"Informe o Arquivo!");
                txtFileDir.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtSaveAs.Text.Trim()))
            {
                MessageBox.Show(@"Informe o Destino!");
                txtSaveAs.Focus();
                return false;
            }
            return true;
        }

        private List<string> ReadXpsFile()
        {
            lblStatus.Text = @"Processando...";
            List<string> lData = new List<string>();
            using (XpsDocument xpsDoc = new XpsDocument(txtFileDir.Text, FileAccess.Read))
            {
                var docSeq = xpsDoc.GetFixedDocumentSequence();
                if (docSeq != null)
                {
                    var pageCount = docSeq.DocumentPaginator.PageCount;

                    progressBar.Maximum = pageCount;
                    progressBar.Value = 0;

                    for (var pageNum = 0; pageNum < pageCount; pageNum++)
                    {
                        var docPage = docSeq.DocumentPaginator.GetPage(pageNum);
                        lData.AddRange(((FixedPage)docPage.Visual).Children.OfType<Glyphs>().Select(uie =>
                        {
                            return (uie).UnicodeString;
                        }));
                        progressBar.Value = pageNum;
                    }
                }
            }

            return lData;
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtFileDir.Text = openFileDialog.FileName;
            }
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtSaveAs.Text = saveFileDialog.FileName;
            }
        }
    }
}